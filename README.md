# Dashboard App

## Setting up development environment
* install [node.js](https://nodejs.org/en/download/)
* install [gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md) & [bower](https://bower.io/)
* if necessary, install jspm-git (although this should be in the package.json already):
    * `npm i -D jspm-git`
    * `npm install -dev jspm`

* execute ``` npm install ```

## Note: Clean Deploy Folder If There Is Any

Various others tasks are there, see gulp folder.

## Run the app in browser

* Start task ```gulp serve```
* Task will run the web on http://localhost:3000
     
The application is now running.
