let setting = {};

setting.resources = {
    'constants': {
        // Common
        'loading': 'Loading',

        // Login Page
        'loginPageTitle': 'Login',
        'usernameLabel': 'Username',
        'passwordLabel': 'Password',
        'usernameRequired': 'Enter a username',
        'passwordRequired': 'Enter a password',
        'invalidCredentials': 'Username or Password is incorrect. Please try again',
        'authenticating': 'Authenticating...',
        'noUserData': 'Could not retrieve account information.',
        'loginSuccessLoading': 'Loading...',
        'internalServerError': 'Internal Server Error',
        'requiredMessage': 'Please fill out this field'
    },
    'links': {
        'login' : 'http://localhost:3000/json/login.json',
        'bookshelf' : 'http://localhost:3000/json/bookshelf.json'
    }
};

export default setting;
