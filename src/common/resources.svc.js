import setting from 'src/common/settings';

const resources = setting.resources;

export function getConstant(key) {
    let constantText;

    constantText = resources.constants[key] ? resources.constants[key] : '';

    return constantText;
}

export function getServiceUrl(key) {
    if (!resources.links[key]) {
        return;
    }

    return resources.links[key];
}

// constants can be used in config blocks
export default angular.module('Resources', [])
    .constant('getConstant', getConstant)
    .constant('getServiceUrl', getServiceUrl)
    .name;
