export default class App {
    constructor($state) {
        'ngInject';
        this.$state = $state;
    }

    $onInit() {
        this.$state.go('login');
    }
}
