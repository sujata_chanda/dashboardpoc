import App from 'src/app/app.ctrl';

export const appComponent = {
    bindings: {},
    template: '<div ui-view></div>',
    controller: App
};
