import uiRouter from 'angular-ui-router';
import resourcesModule from 'src/common/resources.svc';
import loginModule from 'src/login/login.module';
import bookshelfModule from 'src/bookshelf/bookshelf.module';

import {appComponent} from 'src/app/app.component';

const appModule = angular.module('DashboardApp', [
    uiRouter,
    resourcesModule,
    loginModule,
    bookshelfModule
]);

appModule.component('app', appComponent);

export default appModule.name;
