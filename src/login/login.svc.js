class LoginService {
    constructor($http, getServiceUrl) {
        'ngInject';

        this.$http = $http;
        this.getServiceUrl = getServiceUrl;
    }

    verifyLogin() {
        let loginUrl = this.getServiceUrl('login');

        return this.$http.get(loginUrl);
    }
}

export default angular.module('loginService', [])
    .service('loginService', LoginService)
    .name;
