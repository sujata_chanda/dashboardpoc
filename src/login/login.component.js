import template from './login.html!text';

class LoginCtrl {
    constructor(loginService, getConstant, $rootScope, $state) {
        'ngInject';

        this.loginService = loginService;
        this.getConstant = getConstant;
        this.$rootscope = $rootScope;
        this.$state = $state;
    }

    $onInit() {
        this.invalid = {};
        this.$rootscope.bodyClass = 'login';
    }

    submitClick() {
        this.invalid.user = (this.username === undefined || this.username === '');
        this.invalid.pass = (this.password === undefined || this.password === '');

        if (this.invalid.pass) {
            this.focusInput = {
                user: true
            };

            this.invalid.pass = false;
        }

        if (this.invalid.user) {
            this.invalid.user = false;
        }
    }

    onSubmit() {
        this.credentialsErrorStatus = '';

        if (this.invalid.user || this.invalid.pass) {
            return false;
        }

        this.usernameStatus = this.authenticatingStatus = this.invalidCredentialsStatus = '';

        if (!this.username) {
            this.usernameStatus = this.getConstant('usernameRequired');
            return;
        }

        if (!this.password) {
            this.authenticatingStatus = this.getConstant('passwordRequired');
            return;
        }

        this.authenticatingStatus = this.getConstant('authenticating');

        return this.loginService.verifyLogin(this.username, this.password)
            .then(
                response => this.onLoginSuccess(response, {username: this.username, password: this.password}),
                error => this.onLoginFailure(error));
    }

    onLoginSuccess(response, user) {
        let res = response.data;

        if (res[user.username] && res[user.username] === user.password) {
            this.authenticatingStatus = this.getConstant('loginSuccessLoading');
            this.$state.go('bookshelf');
        } else {
            this.authenticatingStatus = '';
            this.credentialsErrorStatus = this.getConstant('invalidCredentials');
        }
    }

    onLoginFailure(error) {
        this.authenticatingStatus = '';
        this.credentialsErrorStatus = this.getConstant('internalServerError');

        let status = error.status;

        if (status >= 400 && status < 500) {
            this.credentialsErrorStatus = this.getConstant('invalidCredentials');
        } else {
            this.credentialsErrorStatus = this.getConstant('internalServerError');
        }
    }
}

const loginComponent = {
    bindings: {},
    template,
    controller: LoginCtrl
};

export default loginComponent;
