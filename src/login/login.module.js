// Module import here
import loginService from './login.svc.js';

// Component import here
import loginComponent from './login.component';

const LoginModule = angular
    .module('LoginModule', [loginService])
    .component('login', loginComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('login', {
                url: '/login',
                component: 'login'
            });
    });

export default LoginModule.name;
