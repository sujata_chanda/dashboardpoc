import 'jquery';
import 'bootstrap';
import 'angular';
import 'angular-ui-router';

import appModule from 'src/app/app.module';

function onReady() {
    angular.bootstrap(document.body, [appModule]);
}

window.addEventListener('DOMContentLoaded', onReady, false);
