import {BookListComponent} from './bookList.component.js';

export default angular.module('bookList', [])
    .component('bookList', BookListComponent)
    .name;
