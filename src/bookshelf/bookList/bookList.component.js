import template from './bookList.html!text';

class BookList {}

export const BookListComponent = {
    bindings: {
        books: '<'
    },
    template,
    controller: BookList
};
