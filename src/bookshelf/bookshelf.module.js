// Module import here
import bookshelfService from './bookshelf.svc.js';
import bookListMod from './bookList/bookList.module.js';

// Component import here
import bookshelfComponent from './bookshelf.component';

const BookshelfModule = angular
    .module('BookshelfModule', [bookshelfService, bookListMod])
    .component('bookshelf', bookshelfComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('bookshelf', {
                url: '/bookshelf',
                component: 'bookshelf'
            });
    });

export default BookshelfModule.name;
