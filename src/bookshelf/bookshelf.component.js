import template from 'src/bookshelf/bookshelf.html!text';

class BookshelfCtrl {
    constructor(bookshelfService, getConstant, $rootScope, $window) {
        'ngInject';

        this.bookshelfService = bookshelfService;
        this.getConstant = getConstant;
        this.$rootscope = $rootScope;
        this.$window = $window;
    }

    $onInit() {
        this.books = [];
        this.$rootscope.bodyClass = 'bookshelf';

        this.setBookshelfHeight();
        this.getBookshelfData();
        this.bindEvents();
    }

    bindEvents() {
        angular.element(this.$window).resize(() => {
            this.setBookshelfHeight();
        });
    }

    getBookshelfData() {
        this.bookshelfService.getBookshelf()
            .then(response => this.onBookshelfSuccess(response),
                error => this.onBookshelfFailure(error));
    }

    setBookshelfHeight() {
        let heightCal = angular.element(this.$window).height() - angular.element('#navBar').height();
        angular.element('#bookshelf-content').height(heightCal);
    }

    onBookshelfSuccess(response) {
        this.books = response.data.books;
    }

    onBookshelfFailure() {
        this.books = [];
    }
}

const bookshelfComponent = {
    bindings: {},
    template,
    controller: BookshelfCtrl
};

export default bookshelfComponent;