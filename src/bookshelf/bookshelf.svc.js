class BookshelfService {
    constructor($http, getServiceUrl) {
        'ngInject';

        this.$http = $http;
        this.getServiceUrl = getServiceUrl;
    }

    getBookshelf() {
        let bookshelfUrl = this.getServiceUrl('bookshelf');

        return this.$http.get(bookshelfUrl);
    }
}

export default angular.module('bookshelfServiceModule', [])
    .service('bookshelfService', BookshelfService)
    .name;