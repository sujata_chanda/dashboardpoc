'use strict';

var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    errorlog = require('gulp-jshint-file-reporter'),
    PATHS = require('../paths.js'),
    path = require('path');

gulp.task('lint', function() {
    return gulp.src([path.join(PATHS.sources, '**', '*.js'),
        // '!src/server/*.js'
    ])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter(errorlog));
});
