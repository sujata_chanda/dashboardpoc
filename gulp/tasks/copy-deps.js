'use strict';

var gulp = require('gulp'),
    jscs = require('gulp-jscs'),
    stylish = require('gulp-jscs-stylish'),
    PATHS = require('../paths.js'),
    targetRoot = PATHS.target,
    scriptsRoot = PATHS.sources,
    flatten = require('gulp-flatten'),
    jspmRoot = PATHS.jspm,
    merge = require('merge-stream'),
    path = require('path');

gulp.task('deps/bootstrap', function() {
    let fonts = gulp.src([`${jspmRoot}/github/twbs/bootstrap*/fonts/*.{eot,svg,ttf,woff,woff2}`])
        .pipe(flatten())
        .pipe(gulp.dest(`${targetRoot}/fonts/`));

    let css = gulp.src([`${jspmRoot}/github/twbs/bootstrap*/css/*.css`])
        .pipe(flatten())
        .pipe(gulp.dest(`${targetRoot}/css/`));

    let js = gulp.src([`${jspmRoot}/github/twbs/bootstrap*/js/bootstrap.min.js`])
        .pipe(flatten())
        .pipe(gulp.dest(`${targetRoot}/js/lib/`));

    return merge(fonts, css, js);
});

gulp.task('web', function() {
    return gulp.src([`${scriptsRoot}/index.html`])
        .pipe(gulp.dest(`${targetRoot}/`));
});

gulp.task('copy-deps', [
    'web',
    'deps/bootstrap']);

