'use strict';

var gulp = require('gulp'),
    jscs = require('gulp-jscs'),
    stylish = require('gulp-jscs-stylish'),
    PATHS = require('../paths.js'),
    path = require('path');

gulp.task('check-style', function() {
    return gulp.src(path.join(PATHS.sources, '**', '*.js'))
        .pipe(jscs('.jscsrc'))
        .pipe(stylish());
});