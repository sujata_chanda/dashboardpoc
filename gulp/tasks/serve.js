'use strict';
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var PATHS = require('../paths.js');

gulp.task('scripts-watch', ['scripts'], browserSync.reload);
gulp.task('less-watch', ['less'], browserSync.reload);

// use default task to launch Browsersync and watch JS files
gulp.task('serve', ['build'], function() {
    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: PATHS.target
        }
    });

    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    gulp.watch(PATHS.sources + '/**/*.js', ['scripts-watch']);
    gulp.watch(PATHS.sources + '/**/*.less', ['less-watch']);
    gulp.watch(PATHS.sources + '/**/*.htm*', ['scripts-watch']);
});
