'use strict';

var gulp = require('gulp'),
    runSequence = require('run-sequence');

gulp.task('build', function(callback) {
    runSequence([
        'lint',
        'copy-img',
        'copy-json',
        'check-style',
        'less',
        'scripts',
        'copy-deps'
    ], callback);
});