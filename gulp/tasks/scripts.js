'use strict';

let gulp = require('gulp'),
    PATHS = require('../paths.js'),
    targetRoot = PATHS.target,
    scriptsRoot = PATHS.sources,
    jspm = require('jspm');

gulp.task('scripts', function() {
    return jspm.bundleSFX(`${scriptsRoot}/main`, `${targetRoot}/js/dashboardApp.js`);
});
