'use strict';

var gulp = require('gulp'),
    less = require('gulp-less'),
    flatten = require('gulp-flatten'),
    ext_replace = require('gulp-ext-replace'),
    PATHS = require('../paths.js');

gulp.task('less', function() {
    return gulp.src(`${PATHS.sources}/**/*.less`)
        .pipe(less({compress: true}))
        .pipe(ext_replace('.min.css'))
        .pipe(flatten())
        .pipe(gulp.dest(`${PATHS.target}/css/`));
});

gulp.task('less-dev', function() {
    return gulp.src(`${PATHS.sources}/**/*.less`)
        .pipe(less({compress: false}))
        .pipe(flatten())
        .pipe(gulp.dest(`${PATHS.target}/css/`));
});
