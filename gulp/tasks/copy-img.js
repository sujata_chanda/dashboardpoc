'use strict';

var gulp = require('gulp'),
    flatten = require('gulp-flatten'),
    PATHS = require('../paths.js'),
    targetRoot = PATHS.target,
    images = PATHS.sourceImages + '/**/*.*';

gulp.task('copy-img', function() {
    // TODO: image optimization
    return gulp.src(images)
        .pipe(flatten())
        .pipe(gulp.dest(targetRoot + '/images/'));
});