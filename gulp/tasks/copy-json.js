'use strict';

var gulp = require('gulp'),
    flatten = require('gulp-flatten'),
    PATHS = require('../paths.js'),
    targetRoot = PATHS.target,
    jsonSource = PATHS.jsonSource + '/**/*.*';

gulp.task('copy-json', function() {
    return gulp.src(jsonSource)
        .pipe(flatten())
        .pipe(gulp.dest(targetRoot + '/json/'));
});