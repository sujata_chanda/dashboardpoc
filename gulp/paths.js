module.exports = {
    sources: 'src',
    target: 'deploy',
    sourceImages: 'src/images',
    jsonSource: 'src/json',
    jspm: 'jspm_packages',
    npm: 'node_modules'
};
